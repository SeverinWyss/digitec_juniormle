from flask import Flask
from flask_restful import reqparse, Resource, Api
from joblib import load
import sklearn
import numpy as np

model_sklearn_verison = "0.24.1"

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('VisitsLastYear')
parser.add_argument('QuestionTextLength')

class Classifier():
    def __init__(self):
        if '{}'.format(sklearn.__version__) != model_sklearn_verison:
            warning = "Warning, the sklearn version({}) differs from the one the model was "+\
                "trained on({}),this might lead to problems!"
            print(warning.format(sklearn.__version__,model_sklearn_verison))
        self.model = load('model/model.joblib')
    
    def classify(self,visitsLastYear,questionTextLength):
        x = np.array([[visitsLastYear,questionTextLength]])
        return {"IsQuestionForCommunity":int(self.model.predict(x)[0])}
        
class Core(Resource):
    def __init__(self):
        super().__init__()
        self.classifier = Classifier()
    
    def get(self):
        return {'hello': 'world'}
        
    def put(self):
        args = parser.parse_args()
        visitsLastYear = int(args['VisitsLastYear'])
        questionTextLength = int(args['QuestionTextLength'])
        return self.classifier.classify(visitsLastYear,questionTextLength)

api.add_resource(Core, '/')

if __name__ == '__main__':
    app.run(debug=True)