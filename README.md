## Prerequisits ##

update-to-date installation of

* Postman,
* docker and 
* python  

project files (git or zip) ready, this is the project root directory

copy your *juniorMLE_dataset.csv* into the directory *project-root/data* 

## SETUP ##

Start console/terminal at the **project root**. 

The instructions here are for Windows.

(Linux Instructions not yet documented, but are fairly similar, pay attention to paths) 


### jupyter part ###

create new virtual environment *venv* and activate it:
> python -m venv venv
> venv\Scripts\activate.bat

update pip:
> (venv)> python -m pip install --upgrade pip

pip install requirements shared by all project elements and the notebook specific one:
> (venv)> pip install -r requirements.txt

> (venv)> pip install -r notebooks\requirements_notebooks.txt

start juypiter
> (venv)> jupyter-lab

Browser should open and then navigate to *notebooks/exercise_A1_simple_model.ipynb*
> *run all cells* (produces *models/model.jpsd*)


### Docker part ###

start a new console/terminal in project root directory (or in the old console/terminal stop jupyter with *crtl+c* and then deactivate the environment *deactivate*):
> docker build . -t sevwys_jml_simple
> docker run -p 3000:5000 -d --name sevwys_jml_simple sevwys_jml_simple


### Postman part ###

* create new Request (name example: sevwyss_jml_simple_app_test)
* change GET to PUT  
* insert url: http://127.0.0.1:3000/
* go to body, 
* select raw, 
* change from Text to JSON and copy past:
> {
>     "VisitsLastYear": 1000,
>     "QuestionTextLength": 54
> }

You will receive an answer in the form:
> {
>     "IsQuestionForCommunity": 1
> }

stop container with: 
>docker stop sevwys_jml_simple
