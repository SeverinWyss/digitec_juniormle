FROM ubuntu:20.04

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

USER root

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y python3
RUN apt-get install -y python3-pip

COPY requirements.txt requirements.txt
RUN pip3 install -r ./requirements.txt --no-cache-dir

COPY requirements_docker.txt requirements_docker.txt
RUN pip3 install -r ./requirements_docker.txt --no-cache-dir

COPY model/model.joblib model/model.joblib

EXPOSE 5000
COPY code/app.py app.py
CMD ["flask", "run"]
